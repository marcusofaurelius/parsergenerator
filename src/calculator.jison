
/* description: Parses and executes mathematical expressions. */

/* lexical grammar */
%lex
%%

\s+                   /* skip whitespace */
[012]                 return 'NUMBER'
"+"                   return '+'
<<EOF>>               return 'EOF'

/lex

/* operator associations and precedence */

%left '+'

%start expressions

%% /* language grammar */

expressions
    : e EOF
        { typeof console !== 'undefined' ? console.log($1) : print($1);
          return $1; }
    ;

e
    : e '+' e
        { }
    | NUMBER
        {$$ = Number(yytext);}
    ;


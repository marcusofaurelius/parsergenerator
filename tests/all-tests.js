exports.testAddParser = require("./plus-test.js");
exports.testMultiParser = require("./multi-test.js");
exports.testEndParser = require("./end-test.js");

if (require.main === module)
    require("test").run(exports);


const calc = require ('../src/end-calc.js');
const assert = require('assert');

exports["test plus brackets calculation"] = function() {
    assert.equal(calc.execute('(2 + 2)'), 4);
}

exports["test divide no space brackets calculation"] = function() {
    assert.equal(calc.execute('10/2'), 5);
}

exports["test plus brackets multiply no spaces calculation"] = function() {
    assert.equal(calc.execute('(2+2)*10'), 40);
}

exports["test multiply brackets calculation"] = function() {
    assert.equal(calc.execute('(10 * 10)'), 100);
}

exports["test multiply brackets around brackets calculation"] = function() {
    assert.equal(calc.execute('((10 * 10))'), 100);
}

exports["test multiply plus brackets calculation"] = function() {
    assert.equal(calc.execute('3 * (3+3)'), 18);
}

exports["test no space brackets minus calculation"] = function() {
    assert.equal(calc.execute('10-2'), 8);
}

exports["test plus too much and no space calculation"] = function() {
    assert.equal(calc.execute('14       +126'), 140);
}

exports["test divide multiply no spaces backets calculation"] = function() {
    assert.equal(calc.execute('20/(2*10)'), 1);
}

exports["test multiply, divide, plus, minus, brackets calculation"] = function() {
    assert.equal(calc.execute('(40 - 4 * 1 - 2 * 4)'), 28);
}

exports["test multiply, divide, plus, minus, double brackets calculation"] = function() {
    assert.equal(calc.execute('40 - ((20 / 10) * 5 * (2 / 4))'), 35);
}

exports["test multiply, divide double brackets, plus, minus calculation"] = function() {
    assert.equal(calc.execute('10 + ((10 / 10)) * 25 - 25 / 5'), 30);
}

exports["test multiply, divide, plus, minus no space, too loose, brackets calculation"] = function() {
    assert.equal(calc.execute('(1100/(10 +1)*(8+8   ) -   1558)'), 42);
};

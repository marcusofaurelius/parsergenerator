
const calc = require ('../src/multi-calc.js');
const assert = require('assert');

exports["test multiply simple calculation"] = function() {
    assert.equal(calc.execute('3 * 3'), 9);
}

exports["test divide simple calculation"] = function() {
    assert.equal(calc.execute('3 / 3'), 1);
}

exports["test minus (ends in 0) calculation"] = function() {
    assert.equal(calc.execute('3 - 3'), 0);
}

exports["test plus, multiply calculation"] = function() {
    assert.equal(calc.execute('3 + 3 * 3'), 12);
}

exports["test multiply, multiple calculation"] = function() {
    assert.equal(calc.execute('10 * 10 * 10 * 10'), 10000);
}

exports["test divide multiple calculation"] = function() {
    assert.equal(calc.execute('20 / 2 / 2'), 5);
}

exports["test minus simple calculation"] = function() {
    assert.equal(calc.execute('10 - 2 '), 8);
}
exports["test plus triple calculation"] = function() {
    assert.equal(calc.execute('14 + 122 + 4'), 140);
}
exports["test divide multiply calculation"] = function() {
    assert.equal(calc.execute('2 / 2 * 10'), 10);
}
exports["test plus, multiply calculation"] = function() {
    assert.equal(calc.execute('2 + 2 * 10'), 22);
}
exports["test minus, multiply, minus, multiply calculation"] = function() {
    assert.equal(calc.execute('40 - 2 * 10 - 2 * 4'), 12);
}
exports["test minus, divide, multi multiply, divide calculation"] = function() {
    assert.equal(calc.execute('40 - 20 / 10 * 5 * 2 / 4'), 35);
}
exports["test plus, divide, multiply, minus calculation"] = function() {
    assert.equal(calc.execute('10 + 10 / 10 * 25 - 25 / 5'), 30);
}
exports["test divide, plus, multiply, plus, minus calculation"] = function() {
    assert.equal(calc.execute('1000 / 10 + 1 * 8 + 8 - 1'), 115);
};

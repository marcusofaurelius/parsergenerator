const calc = require ('../src/plus-calc.js');
const assert = require('assert');

    exports["test plus simple calculation"] = function() {
        assert.equal(calc.execute('3 + 3'), 6);
    }
    exports["test plus 0 calculation"] = function() {
        assert.equal(calc.execute('0 + 800'), 800);
    }
    exports["test plus 0 and 0 calculation"] = function() {
        assert.equal(calc.execute('0 + 0'), 0, "summary");
    }
    exports["test plus triple digits calculation"] = function() {
        assert.equal(calc.execute('14 + 126'), 140, "summary");
    }
    exports["test plus big digits calculation"] = function() {
        assert.equal(calc.execute('10000000000 + 2908098'), 10002908098, "summary");
    };

